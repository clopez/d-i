# translation of debian-installer_packages_po.po to Arabic
# Arabic messages for debian-installer. Copyright (C) 2003 Software in the Public Interest, Inc. This file is distributed under the same license as debian-installer. Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2006, 2007, 2008, 2009, 2010.
# Ossama Khayat <okhayat@yahoo.com>, 2011, 2013.
# ButterflyOfFire <ButterflyOfFire@protonmail.com>, 2018.
# Osama <osama.ibrahim.89@gmail.com>, 2019.
# Fahim Sabah <katheer.kaleel@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-23 20:01+0000\n"
"PO-Revision-Date: 2023-02-10 08:40+0000\n"
"Last-Translator: Khaled Mahmoud <Khaled.q6@gmail.com>\n"
"Language-Team: \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 4.16-dev\n"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
#, fuzzy
#| msgid "Use non-free software?"
msgid "Use non-free firmware?"
msgstr "تريد استخدام البرامج الغير حرّة؟"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
#, fuzzy
#| msgid ""
#| "Some non-free software has been made to work with Debian. Though this "
#| "software is not at all a part of Debian, standard Debian tools can be "
#| "used to install it. This software has varying licenses which may prevent "
#| "you from using, modifying, or sharing it."
msgid ""
"Some non-free firmware has been made to work with Debian. Though this "
"firmware is not at all a part of Debian, standard Debian tools can be used "
"to install it. This firmware has varying licenses which may prevent you from "
"using, modifying, or sharing it."
msgstr ""
"بعض البرامج الغير حرّة صنعت لتعمل مع دبيان. مع أن هذه البرامج ليست إطلاقاً جزء "
"من دبيان، فيمكن استخدام أدوات دبيان الاعتيادية لتثبيتها. هذه البرامج لها رخص "
"مختلفة قد تمنعك من استخدامها، أو تعديلها، أو التشارك بها."

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:58001
#, no-c-format
msgid "ZFS pool %s, volume %s"
msgstr "مجموعة ZFS %s، الكتلة %s"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:60001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:61001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s)، الجزء #%s"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:60001
msgid ""
"Your boot partition has not been configured with the ext2 file system. This "
"is needed by your machine in order to boot. Please go back and use the ext2 "
"file system."
msgstr ""
"جزء الإقلاع لم يُهيّأ باستخدام نظام ملفّات ext2. يتطلّب جهازك هذا كي يتمكّن من "
"الإقلاع. رجاءً عُد واستخدم نظام الملفّات ext2."

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:61001
msgid ""
"Your boot partition is not located on the first partition of your hard disk. "
"This is needed by your machine in order to boot.  Please go back and use "
"your first partition as a boot partition."
msgstr ""
"جزء الإقلاع لا يقع في الجزء الأوّل من قرصك الصّلب. يتطلب جهازك ذلك ليتمكّن من "
"الإقلاع.  رجاء ارجع واستخدم الجزء الأوّل كجزء إقلاع."

#. Type: text
#. Description
#. :sl5:
#. Setting to reserve a small part of the disk for use by BIOS-based bootloaders
#. such as GRUB.
#: ../partman-partitioning.templates:32001
msgid "Reserved BIOS boot area"
msgstr "منطقة إقلاع BIOS محجوزة"

#. Type: text
#. Description
#. :sl5:
#. short variant of 'Reserved BIOS boot area'
#. Up to 10 character positions
#: ../partman-partitioning.templates:33001
msgid "biosgrub"
msgstr "biosgrub"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "ctc: Channel to Channel (CTC) or ESCON connection"
msgstr "ctc: اتّصال قناة لقناة (CTC) أو ESCON"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "qeth: OSA-Express in QDIO mode / HiperSockets"
msgstr "qeth: OSA-إكسبرس في وضع QDIO / هايبرسوكتس"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "iucv: Inter-User Communication Vehicle - available for VM guests only"
msgstr "iucv: مركبة اتّصال بين المستخدمين - متوفّر لضيوف VM فقط"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "virtio: KVM VirtIO"
msgstr "virtio: KVM VirtIO"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid "Network device type:"
msgstr "نوع جهاز الشبكة:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid ""
"Please choose the type of your primary network interface that you will need "
"for installing the Debian system (via NFS or HTTP). Only the listed devices "
"are supported."
msgstr ""
"الرجاء اختيار نوع واجهة الشبكة الأوّليّة التي ستحتاجها لتثبيت نظام ديبيان (عبر "
"NFS أو HTTP). الأجهزة المسردة فقط هي المدعومة."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001
msgid "CTC read device:"
msgstr "جهاز قراءة CTC:"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001 ../s390-netdevice.templates:3001
msgid "The following device numbers might belong to CTC or ESCON connections."
msgstr "أرقام الأجهزة التالية فد تكون تابعة لوصلات CTC أو ESCON."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:3001
msgid "CTC write device:"
msgstr "جهاز كتابة CTC:"

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001 ../s390-netdevice.templates:8001
#: ../s390-netdevice.templates:12001
msgid "Do you accept this configuration?"
msgstr "هل هذه التهيئة مقبولة؟"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001
msgid ""
"The configured parameters are:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"
msgstr ""
"المعطيات المهيئة هي:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "No CTC or ESCON connections"
msgstr "لا اتّصالات CTC أو ESCON"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "Please make sure that you have set them up correctly."
msgstr "رجاءً تأكّد أنك قمت بإعدادها بشكل صحيح."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:6001
msgid "Protocol for this connection:"
msgstr "البروتوكول المستخدم لهذا الاتّصال:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Device:"
msgstr "الجهاز:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Please select the OSA-Express QDIO / HiperSockets device."
msgstr "الرجاء اختيار جهاز OSA-Express QDIO / HiperSockets."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:8001
msgid ""
"The configured parameters are:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" layer2   = ${layer2}"
msgstr ""
"الإعدادات التي تم تجهيزها :\n"
" القنوات    = ${device0}, ${device1}, ${device2}\n"
" المنفذ     = ${port}\n"
" الطبقة 2   = ${layer2}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid "No OSA-Express QDIO cards / HiperSockets"
msgstr "لا بطاقات OSA-Express QDIO / HiperSockets"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid ""
"No OSA-Express QDIO cards / HiperSockets were detected. If you are running "
"VM please make sure that your card is attached to this guest."
msgstr ""
"لم تكتشف أيّة بطاقات OSA-Express QDIO / أو HiperSockets. إن كنت تستخدم VM "
"فرجاءً تأكّد أن بطاقتك متّصلة بهذا الضيف."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Port:"
msgstr "منفذ:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Please enter a relative port for this connection."
msgstr "الرجاء أدخل منفذاً نسبيّا لهذا الاتصال."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Use this device in layer2 mode?"
msgstr "استخدام هذا الجهاز في وضع layer2؟"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid ""
"By default OSA-Express cards use layer3 mode. In that mode LLC headers are "
"removed from incoming IPv4 packets. Using the card in layer2 mode will make "
"it keep the MAC addresses of IPv4 packets."
msgstr ""
"تستخدم بطاقات OSA-Express وضع layer3 بشكل افتراضي. وفي هذا الوضع تتم إزالة "
"ترويسات LLC من حزمات IPv4 الواردة. استخدام البطاقة بوضع layer2 سيجعلها تبقي "
"عناوين MAC في حزمات IPv4."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid ""
"The configured parameter is:\n"
" peer  = ${peer}"
msgstr ""
"المعطى المعدّ هو:\n"
" peer  = ${peer}"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "VM peer:"
msgstr "ند VM:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "Please enter the name of the VM peer you want to connect to."
msgstr "الرجاء إدخال اسم ند VM التي تريد الاتّصال بها."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"If you want to connect to multiple peers, separate the names by colons, e."
"g.  tcpip:linux1."
msgstr ""
"إن كنت تريد الاتصال بعدّة نقاط، فافصل الأسماء بقولون، مثلاً، tcpip:linux1."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"The standard TCP/IP server name on VM is TCPIP; on VIF it's $TCPIP. Note: "
"IUCV must be enabled in the VM user directory for this driver to work and it "
"must be set up on both ends of the communication."
msgstr ""
"الاسم المثالي لخادم TCP/IP على VM هو TCPIP؛ على VIF فهو $TCPIP. ملاحظة: يجب "
"أن يكون IUCV ممكّناً في دليل المستخدم على VM كي يعمل هذا المُعرّف ويجب أن يكون "
"معيّناً على كلا نقطتي الاتّصال."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "Configure the network device"
msgstr "تهيئة جهاز الشبكة"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available devices:"
msgstr "الأجهزة المتوفرة:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following direct access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"أجهزة تخزين الوصول المباشر (DASD) التالية متوفرة. الرجاء اختيار كل واحد تريد "
"استخدامه على حدة."

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "اختر \"إنهاء\" في أسفل القائمة عندما تنتهي."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose device:"
msgstr "اختر الجهاز:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a device. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"الرجاء اختيار جهاز. يجب أن تحدد رقم الجهاز الكامل، بما في ذلك الأصفار "
"البادئة."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid device"
msgstr "جهاز غير صالح"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "لقد تمّ اختيار رقم جهازٍ غير صالح."

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001 ../s390-dasd.templates:5001
msgid "Format the device?"
msgstr "أتريد تنسيق الجهاز؟"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "The DASD ${device} is already low-level formatted."
msgstr "الجهاز DASD ${device} تم تنسيقه بالفعل على مستوى منخفض."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"Please choose whether you want to format again and remove any data on the "
"DASD. Note that formatting a DASD might take a long time."
msgstr ""
"من فضلك قم باختيار ما إذا كنت ترغب في التهيئة مرة أخرى وإزالة أي بيانات على "
"DASD. ملاحظة تهيئة DASD من الممكن أن يستغرق وقتا طويلا."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid ""
"The DASD ${device} is not low-level formatted. DASD devices must be "
"formatted before you can create partitions."
msgstr ""
"جهاز DASD ${device} لم يتم تنسيقه مسبقاً. يجب تنسيق الجهاز قبل أن يمكن إنشاء "
"أجزاء عليه."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "The DASD ${device} is in use"
msgstr "الجهاز DASD ${device} قيد الاستخدام حالياً"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid ""
"Could not low-level format the DASD ${device} because the DASD is in use.  "
"For example, the DASD could be a member of a mapped device in an LVM volume "
"group."
msgstr ""
"تعذّر تنسيق جهاز ${device} ذي الدخول المباشر DASD لأنه قيد الاستعمال. مثلا "
"يمكن أن يكون الجهاز عنصرا في جهاز مرسَّم في مجموعة كتلة منطقية LVM."

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:7001
msgid "Formatting ${device}..."
msgstr "تنسيق ${device}..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:8001
msgid "Configure direct access storage devices (DASD)"
msgstr "تهيئة أجهزة تخزين الوصول المباشر (DASD)"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../zipl-installer.templates:1001
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "تثبيت محمّل الإقلاع ZIPL على قرصٍ صلب"
